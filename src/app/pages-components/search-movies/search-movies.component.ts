import {Component, OnInit} from '@angular/core';
import {ThemoviedbBusinessService} from "../../core/business-services/themoviedb-business.service";
import {MoviesItemsDto} from "../../core/models/movies-items.dto";

@Component({
    selector: 'app-search-movies',
    templateUrl: './search-movies.component.html',
    styleUrls: ['./search-movies.component.scss']
})
export class SearchMoviesComponent implements OnInit {

    searchInput: string | undefined;

    loaded: boolean = true;
    inError: boolean = false;
    pageIndex: number = 1;
    searchedMovies: MoviesItemsDto | undefined;

    constructor(
        private themoviedbBusinessService: ThemoviedbBusinessService,
    ) {
    }

    ngOnInit(): void {
        this.loaded = true;
    }

    onClickSearchMovies(): void {
        this.loaded = false;
        this.inError = false;
        this.searchedMovies = undefined;
        if (this.searchInput) {
            this.themoviedbBusinessService.searchMoviesByKeyworksAndPageNumber(
                this.searchInput,
                this.pageIndex
            ).subscribe(
                result => {
                    this.searchedMovies = result;
                    setTimeout(() => {
                        this.loaded = true;
                    }, 1000);
                },
                error => {
                    this.inError = true;
                }
            );
        }
    }

    setPageNumberAndSearchMovies(pageNumber: number) {
        this.pageIndex = pageNumber;
        this.onClickSearchMovies();
    }

}
