import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Router} from "@angular/router";
import {APP_NAVIGATIONS_ROUTES} from "../../app.constants";


@Component({
    selector: 'app-top-nav-bar',
    templateUrl: './top-nav-bar.component.html',
    styleUrls: ['./top-nav-bar.component.scss']
})
export class TopNavBarComponent implements OnInit {

    @Output() toggleSideNavBarBtnOutputEmitter = new EventEmitter<any>();

    constructor(
        private router: Router,
    ) {
    }

    ngOnInit(): void {
    }

    onClickToggleSideNavBarBtnHandler(): void {
        this.toggleSideNavBarBtnOutputEmitter.emit();
    }

    onClickHomeBtnHandler() {
        this.router.navigate([
            APP_NAVIGATIONS_ROUTES.HOME
        ]);
    }

    onClickPopularMoviesBtnHandler() {
        this.router.navigate([
            APP_NAVIGATIONS_ROUTES.POPULAR_MOVIES
        ]);
    }

}
