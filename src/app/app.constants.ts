
export const THE_MOVIE_DB_API_BASE_URL = 'https://api.themoviedb.org/3';
export const THE_MOVIE_DB_API_KEY = 'd6fe1b6e6f0edf5d4ca9489da8615f1f';
export const THE_MOVIE_DB_LANGAUGE = 'fr';
export const THE_MOVIE_DB_POSTER_BASE_URL = 'https://image.tmdb.org/t/p/w500';

export const APP_DOMAINE_PATH = 'mdb';

export const APP_MAIN_ROUTES = {
    HOME: 'home',
    POPULAR_MOVIES: 'popular-movies',
    SEARCH_MOVIES: 'search-movies',
    MOVIE_DETAILS: 'movie-details/:movieId',
    APP_ERROR: 'error',
    NOT_FOUND_ERROR: 'not-found',
}

export const APP_NAVIGATIONS_ROUTES = {
    HOME: `${APP_DOMAINE_PATH}/${APP_MAIN_ROUTES.HOME}`,
    POPULAR_MOVIES: `${APP_DOMAINE_PATH}/${APP_MAIN_ROUTES.POPULAR_MOVIES}`,
    SEARCH_MOVIES: `${APP_DOMAINE_PATH}/${APP_MAIN_ROUTES.SEARCH_MOVIES}`,
    MOVIE_DETAILS: `${APP_DOMAINE_PATH}/${APP_MAIN_ROUTES.MOVIE_DETAILS}`,
    APP_ERROR: `${APP_DOMAINE_PATH}/${APP_MAIN_ROUTES.APP_ERROR}`,
    NOT_FOUND_ERROR: `${APP_DOMAINE_PATH}/${APP_MAIN_ROUTES.NOT_FOUND_ERROR}`,
}
